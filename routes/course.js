const express = require ("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require('../auth');

// Route for creating a course
router.post("/",  auth.verify,(req, res) => {
	const courseData = auth.decode(req.headers.authorization)

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})


// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllcourses().then(resultFromController => res.send(resultFromController));
})


// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})


// Route fr updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


router.put("/:courseId",auth.verify,(req,res)=>{
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(auth.decode(req.headers.authorization).isAdmin){
	courseController.archiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
	}else{res.send('not authorized')}
})

module.exports = router;

