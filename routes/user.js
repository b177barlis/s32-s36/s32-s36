const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require('../auth');

// Route checking if the user's email already exists in the database

// Get all users
router.get("/", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
})


// endpoint: localhost:4000/users/checkEmail
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for registering a user.
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.get("/details", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)

	userController.getUserById({userId: userData.id}).then(resultFromController=> res.send(resultFromController))
})

// Route to enroll 
router.post("/enroll", auth.verify,(req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})



module.exports = router;