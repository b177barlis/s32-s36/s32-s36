const Course = require("../models/Course");
const auth = require('../auth');

module.exports.addCourse = (reqBody) => {
	// Create a variable "newCourse" and instantiates a new "Course" object
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	})


	return newCourse.save().then((course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		};
	})

}


// Controller function for getting all courses
module.exports.getAllcourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
} 

// Controller

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Controller function for updating course
module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.archiveCourse= (reqParams,reqBody)=>{
	let archivedCourse = {isActive: reqBody.isActive }
	return Course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error)=>{
		if(error){return false}
		else {return true}
	})
}
